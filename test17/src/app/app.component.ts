import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'test17';
  showMessage:string='';

  price:number;
  stock:string;
   
  stockApp:string;
  onNotify(message:string) {
    this.showMessage = message;
  }

  // onInputEvent(event:Event):void { }

  onInputEvent({target}):void { 
    this.stockApp = target.value;
  }

  priceQuoteHandler(event: CustomEvent) {
    this.stock = event.detail.stock;
    this.price = event.detail.lastPrice;
    
  }
}
