import { Component, OnInit, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import { FavoriteComponent } from '../favorite/favorite.component';

interface IPriceQuoter {
  stock: string;
  lastPrice: number;
}

@Component({
  selector: 'app-price-quoter',
  template: '<b> Inside Price Quoter {{stock}} {{ price | currency: "EUR": true: "1.2-2"}} </b>',
  styles: [':host{ background: yellow;}']
})
export class PriceQuoterComponent implements OnInit {
 //@Output() lastPrice: EventEmitter<IPriceQuoter> = new EventEmitter();

 //@Output('last-price') lastPrice;
 
  stock: string = 'Samsung ';
  price: number;
  
  constructor(element: ElementRef) {
    setInterval(() => { 
      let priceQuote: IPriceQuoter = {
        stock: this.stock,
        lastPrice: 100 * Math.random()
      };

      this.price = priceQuote.lastPrice;
      element.nativeElement.dispatchEvent( new CustomEvent('last-price', {
        detail: priceQuote,
        bubbles:true
      }))
    }, 1000);
  }

  // constructor() { 
  //   setInterval(() => {
  //     let priceQuote: IPriceQuoter = {
  //       stock: this.stock,
  //       lastPrice: 100 * Math.random()
  //     };

  //     this.price = priceQuote.lastPrice;
  //     //this.lastPrice.emit(priceQuote);
  //     console.log(' Last price >>> ',this.price );
      
  //   }, 1000)

  // }

  ngOnInit(): void {
  }
   
}
