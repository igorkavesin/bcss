import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
 

@Component({
  selector: 'app-favorite',
  template: `
      <button (click)="onClick()"> Click me </button>
      {{quantity}} shared of {{stock}}
      ` ,
  styles: [`:host { 
      background:#77aaff;
      padding:10px; 
      margin:10px;
      border:1px solid gray;
    }`
  ]
})
export class FavoriteComponent implements OnInit {

  @Input() reviews: number;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  @Input() quantity: number;
  private _stock: string;

  @Input() set stock(value: string) {
    this._stock = value;
    console.log('quantity >>> ',  
      (this.quantity == undefined ? 0 : `${this.quantity}`) );
  }

  get stock():string {
    return this._stock;
  }

  constructor() { 
    this.stock ="constructor";
  }

  ngOnInit(): void { this.stock ="ngOnInit"; }

  onClick(): void {
    this.notify.emit('hello this is me emit!!! this message from child!!!');
  }

}
