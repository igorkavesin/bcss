import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Car } from '../car.model';
 

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {
  title= '';
  @Input() inputcar: Car
  @Output() deleteCar = new EventEmitter<Car>()

  constructor() { 
    this.title= 'CAR';
  }

  ngOnInit(): void {
  }

  onDelete() {

    this.deleteCar.emit(this.inputcar);
  }

  onBuy() {
    this.inputcar.isSold = true;
  }

}
