import { Component } from '@angular/core';
import { Cars, Car } from './car.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  public cars: Car[] = [
    new Car('ford', '12.12.12', 'Focus', true, 1),
    new Car('peugeot', '12.12.12', '5008', false, 2)
  ];


  onAdd(car: Car) {

    this.cars.push(car);
  }
  onDelete(car:Car) {
     
    this.cars = this.cars.filter(c => c.id !==car.id);
  }
}
