import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'test16';
  flag: boolean = true;
  lastSymbol: string;

  greeting: string = ' hello !!!';

  message1: string;
  message2: string;
  message3: string;

  tempf: number;
  toCelsius: boolean= true;
  targgetFormat:string ='Celcius';
  format: string = 'FtoC';

  constructor() {
    setTimeout(() => {
        this.lastSymbol = ( Math.random().toFixed(4));
    },  1000);
  }

  toggleFormat(): void {
    
    this.toCelsius = !this.toCelsius;
    this.format = this.toCelsius ? 'FtoC' : 'CtoF';
    this.targgetFormat = this.toCelsius ? 'Celcius': 'Fahrenheit';

  }

  onInputEvent(event: Event) {

    let onInputEvent: HTMLInputElement = <HTMLInputElement> event.target;

    this.message1 = onInputEvent.value;
    this.message2 = onInputEvent.getAttribute('value');
    this.message3 = this.greeting;
  }
}
