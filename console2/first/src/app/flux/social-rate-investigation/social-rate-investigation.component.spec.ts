import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialRateInvestigationComponent } from './social-rate-investigation.component';

describe('SocialRateInvestigationComponent', () => {
  let component: SocialRateInvestigationComponent;
  let fixture: ComponentFixture<SocialRateInvestigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialRateInvestigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialRateInvestigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
