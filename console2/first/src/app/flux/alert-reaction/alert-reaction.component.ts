import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, } from '@angular/forms';
import { AppSettings } from '../../shared/app-settings';
import { INavigationParams } from '../../shared/inavigation';
import { IForm } from '../../shared/iform';

@Component({
  selector: 'app-alert-reaction',
  templateUrl: './alert-reaction.component.html'
})
export class AlertReactionComponent implements OnInit {

    @Input() alertid: number;
    @Input() reference: number;
    @Input() motivation: number;
    @Input() comment: number;

    public dispalyTitle = 'AlertReaction';
    public dispalyDesc = '- D\'envoyer une motivation clignotant à la BCSS. ';
    public cookieValue: any;
    public hint: boolean;
    public form: FormGroup;
    private pattern: any;

    private data = {
      alertid:    this.alertid,
      reference:  this.reference,
      motivation: this.motivation,
      comment:    this.comment
    };

  public constructor() { }

  public ngOnInit(): void {
    const formData = {};
    for (const prop of Object.keys(this.data)) {
      this.pattern = (
        prop === 'comment' ?
        AppSettings.REGEX.STRING_SIMPLE :
        AppSettings.REGEX.NUMBER_SIMPLE
      );
      formData[prop] = new FormControl(
        this.data[prop],
        [ Validators.required, Validators.pattern(this.pattern) ]
      );
    }
    this.form = new FormGroup(formData);
  }

  public getValueFromInputApi(): IForm {
    return {
      callValueFromInput: (name) => {
        return this.getValueFromInput(name);
      }
    };
  }

  public getValueFromInput(name: string): string {
    return 'alert_reaction/consulter/?env=prod' +
      '&alertId=' + this.form.get('alertid').value +
      '&formNumber=' + this.form.get('reference').value +
      '&codeM=' + this.form.get('motivation').value +
      '&comment=' + this.form.get('comment').value ;
  }

  public getParamsy(): INavigationParams[] {
    const params: INavigationParams[] = [];
    const add: INavigationParams = {context: 'test', env: 'asdasd', route: ''};
    params.push(add);
    return params;
  }

}
