import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivingWagesComponent } from './living-wages.component';

describe('LivingWagesComponent', () => {
  let component: LivingWagesComponent;
  let fixture: ComponentFixture<LivingWagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivingWagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivingWagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
