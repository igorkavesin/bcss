import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfAttestationComponent } from './list-of-attestation.component';

describe('ListOfAttestationComponent', () => {
  let component: ListOfAttestationComponent;
  let fixture: ComponentFixture<ListOfAttestationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfAttestationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfAttestationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
