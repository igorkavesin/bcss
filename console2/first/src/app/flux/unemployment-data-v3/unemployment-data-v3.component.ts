import { Component, OnInit, Input } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import { Router } from '@angular/router';

import {FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup, AbstractControl} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { MatSlideToggle, MatSlideToggleChange, MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';

import { Observable} from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map, catchError, tap , delay } from 'rxjs/operators';

@Component({
  selector: 'app-unemployment-data-v3',
  templateUrl: './unemployment-data-v3.component.html',
  styleUrls: ['./unemployment-data-v3.component.scss']
})
export class UnemploymentDataV3Component implements OnInit {

  @Input() rn: number;

  private data = {
    rn: this.rn,
  };

  public hint = false;
  private numberRegEx = /^\d+$/;
  public loading = false;
  private url = 'http://10.10.0.62/php5/bcss-test/api/';
  public form: FormGroup;

  navLinks: any[];

  // tslint:disable-next-line: variable-name
  private _activeLinkIndex = -1;

  public get activeLinkIndex() {
    return this._activeLinkIndex;
  }

  public set activeLinkIndex(value) {
    this._activeLinkIndex = value;
  }

  public show: any;
  public tab: any;

  constructor(private router: Router, public dialog: MatDialog, private http: HttpClient) { }

  ngOnInit() {
    const formData = {};
    for ( const prop of Object.keys(this.data)) {
      formData[prop] = new FormControl(
          this.data[prop],
          [ Validators.required, Validators.pattern(this.numberRegEx) ]
      );
    }
    this.form = new FormGroup(formData);

  //   this.router.events.subscribe((res) => {
  //     this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
  // });
  }

  tabChanged(event: any) {
    console.log(event);
  }

  public btnSend(): void {
    this.doRequest();
  }

  doRequest(): never {
    throw new Error('Method not implemented.');
  }

  public btnClear(): void {
    this.form.reset();
  }

  public onChange(ob: MatSlideToggleChange): void {
    console.log(ob.checked);
    const matSlideToggle: MatSlideToggle = ob.source;
    console.log(matSlideToggle.color);
    console.log(matSlideToggle.required);
    this.hint = ob.checked;
  }


}
