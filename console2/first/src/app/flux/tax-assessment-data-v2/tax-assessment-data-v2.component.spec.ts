import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAssessmentDataV2Component } from './tax-assessment-data-v2.component';

describe('TaxAssessmentDataV2Component', () => {
  let component: TaxAssessmentDataV2Component;
  let fixture: ComponentFixture<TaxAssessmentDataV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxAssessmentDataV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAssessmentDataV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
