import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandiFluxComponent } from './handi-flux.component';

describe('HandiFluxComponent', () => {
  let component: HandiFluxComponent;
  let fixture: ComponentFixture<HandiFluxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandiFluxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandiFluxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
