

import { Component, OnInit, Input, ɵConsole, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { MatSlideToggle, MatSlideToggleChange, MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../../shared/app-settings';
import { INavigation, INavigationParams } from '../../shared/inavigation';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { RequestService } from '../../shared/request.service';

export interface IScope {
  value: string;
  viewValue: string;
}

const rnLocal = 'rn';
type btnId = string;

@Component({
  selector: 'app-dolsis',
  templateUrl: './dolsis.component.html',
  styleUrls: ['./dolsis.component.scss']
})

export class DolsisComponent implements OnInit {

  @Input() rn: number;
  public cookieValue: any;
  private data = {
    rn:    this.rn,
  };

  public hint = false;
  public loading = false;
  public form: FormGroup;
  private btn: btnId = 'btn_send';

  scopes: IScope[] = [
    {value: 'active', viewValue: 'Active'},
    {value: 'activeInPeriod',  viewValue: 'ActiveInPeriod'},
    {value: 'mutations', viewValue: 'Mutations'}
  ];

public constructor(public req: RequestService, public dialog: MatDialog, private http: HttpClient, private router: Router) { }

  ngOnInit() {
    const formData = {};
    formData[rnLocal] = new FormControl(
      this.data[rnLocal],
      [ Validators.required, Validators.pattern(AppSettings.REGEX.RN) ]
    );
    this.form = new FormGroup(formData);
  }

  public doRequest(): void {}

  public btnSend(): void {
    (document.getElementById(this.btn) as HTMLInputElement).disabled = true;
    this.form.disable();
    this.doRequest();
  }

  public btnClear(): void {
    this.form.reset();
  }

  public onChange(ob: MatSlideToggleChange): void {
    console.log(ob.checked);
    let matSlideToggle: MatSlideToggle = ob.source;
    console.log(matSlideToggle.color);
    console.log(matSlideToggle.required);
    this.hint = ob.checked;

    console.log('====> ');

  }

}
