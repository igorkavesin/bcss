import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfEmployedV2Component } from './self-employed-v2.component';

describe('SelfEmployedV2Component', () => {
  let component: SelfEmployedV2Component;
  let fixture: ComponentFixture<SelfEmployedV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfEmployedV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfEmployedV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
