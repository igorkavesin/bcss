import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatSlideToggleChange, MatSlideToggle } from '@angular/material';

@Component({
  selector: 'app-slide-toggle-error',
  template: `
    <mat-slide-toggle warn (change)="onChange($event)">
      <span *ngIf="hint" class="act">Cacher des erreurs</span>
      <span *ngIf="!hint" class="inact">Afficher des erreurs </span>
    </mat-slide-toggle> `
})
export class SlideToggleErrorComponent implements OnInit {

  @Input() public hint = false;
  @Output() public EventSlideToggle = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  public onChange(ob: MatSlideToggleChange): void {
    const matSlideToggle: MatSlideToggle = ob.source;
    console.log(matSlideToggle.color);
    console.log(matSlideToggle.required);
    this.hint = ob.checked;
    this.EventSlideToggle.emit(ob.checked);
  }

}
