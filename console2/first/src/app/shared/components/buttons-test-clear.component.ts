import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IForm } from '../iform';
import { AppSettings } from '../../shared/app-settings';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { RequestService } from '../request.service';
import { MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-buttons-test-clear',
  template: `
    <app-progress-bar [loading]="loading"></app-progress-bar>
    <button mat-raised-button color="primary" (click)="btnSend()"
        class="submit-button" id="idsend"
        [disabled]="form.invalid">
      <mat-icon matSuffix>input</mat-icon> Tester
    </button>

    <button mat-raised-button color="ink-ripple" (click)="btnClear()">
      <mat-icon matSuffix >subject</mat-icon> Effacer
    </button>`,
})
export class ButtonsTestClearComponent {

  @Input() public form: FormGroup;
  @Input() public formApi: IForm;

  public loading = false;
  private idsend = 'idsend';
  private path: string;

  constructor(public req: RequestService, public dialog: MatDialog, private http: HttpClient, private router: Router) { }

  private doRequest() {
    this.req.handleTheRequest(AppSettings.URL + this.path).subscribe(
      (response)  => {
        (document.getElementById(this.idsend) as HTMLInputElement).disabled = false;
        this.form.enable();
        this.loading = false;

        const dialogRef = this.dialog.open( ConfirmDialogComponent, {
          data: { bcss: response, full: this.form.get }
        });
        dialogRef.afterClosed();
      },
      (error) => { console.log(' ERROR : ',  error); },
      () => { console.log(' COMPLECT!!! '); }
    );
  }

  public btnSend() {
    this.path = this.formApi.callValueFromInput('');
    (document.getElementById(this.idsend) as HTMLInputElement).disabled = true;
    this.form.disable();
    this.loading = true;
    this.doRequest();
  }

  public btnClear(): void {
    this.form.reset();
  }

}

