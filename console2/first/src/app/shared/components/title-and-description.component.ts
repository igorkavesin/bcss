import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-title-and-description',
  template: `
    <div class="fluxTitle ng-binding">
      <mat-icon matSuffix>compare_arrows</mat-icon>
      <span style="font-size: 22px;"> {{ dispalyTitle }} </span>
      <span> {{ dispalyDesc }} </span>
    </div> `
})
export class TitleAndDescriptionComponent implements OnInit {

  @Input() public dispalyTitle;
  @Input() public dispalyDesc;

  constructor() { }

  ngOnInit() {
  }

}
