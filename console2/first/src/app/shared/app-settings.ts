
export class AppSettings {
    public static readonly URL = '/php5/bcss-test/api/';
    public static readonly REGEX = {
      NUMBER_SIMPLE: /^\d+$/,
      STRING_SIMPLE: /^[a-zA-Z0-9\_\,\+\;\-\.\=\!\?\:]{0,1000}$/,
      // tslint:disable-next-line: max-line-length
      RN: /^[0-9]{11}$/
    };
    public static readonly DIALOG_CONF = {
      WIDTH: '20% !important',
      HEIGHT: '20% !important'
    }
    public static readonly TIMING = 1000;
  }
