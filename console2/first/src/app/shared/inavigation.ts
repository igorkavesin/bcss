export interface INavigationParams {
  env?: string;
  context?: string;
  route?: string;
}
export interface INavigation {
    displayName: string;
    disabled?: boolean;
    iconName: string;
    route?: string;
    position?: number;
    env?: string;
    children?: INavigation[];
}
export class NavigMenu {

  navigList(): INavigation[] {
    return [
      {
        displayName: 'Legal Context', iconName: 'shuffle', children: [
          { displayName: 'PCSA:SOCIAL_INQUIRY', iconName: 'more_horiz', route: ' ', position: 1 },
          { displayName: 'PCSA:MANAGE_ECARMED', iconName: 'more_horiz', route: ' ', position: 1 },
          {  displayName: 'SOCIAL_INVESTIGATION_OF_PCSA', iconName: 'more_horiz', route: ' ', position: 1 },
        ]
      },
      {
        displayName: 'Environements', iconName: 'share', children: [
          { displayName: 'Production', iconName: 'more_horiz',  route: ' ', position: 2, env: 'prod' },
          { displayName: 'Acceptation', iconName: 'more_horiz', route: ' ', position: 2, env: 'acpt' },
          { displayName: 'Test',  iconName: 'more_horiz', route: ' ', position: 2, env: 'test' }
        ]
      },
      {
        displayName: 'Flux', iconName: 'compare_arrows',
        children: [
          { displayName: 'AlertReaction', iconName: 'more_horiz', route: 'alert-reaction' },
          { displayName: 'Cadnet', iconName: 'more_horiz', route: 'cadnet' },
          { displayName: 'ChildBenefits', iconName: 'more_horiz', route: 'child-benefits' },
          { displayName: 'Dimona', iconName: 'more_horiz', route: 'dimona' },
          { displayName: 'Dolsis', iconName: 'more_horiz', route: 'dolsis' },
          { displayName: 'FamilyAllowancesService', iconName: 'more_horiz', route: 'family-allowances-service' },
          { displayName: 'HandiFlux', iconName: 'more_horiz', route: 'handi-flux' },
          { displayName: 'HealthCareInsurance', iconName: 'more_horiz', route: 'health-care-insurance' },
          { displayName: 'IdentifyPerson', iconName: 'more_horiz', route: 'identify-person' },
          { displayName: 'ListOfAttestation', iconName: 'more_horiz', route: 'list-of-attestation' },
          { displayName: 'LivingWages', iconName: 'more_horiz', route: 'living-wages' },
          { displayName: 'ManageAccess', iconName: 'more_horiz', route: 'manage-access' },
          { displayName: 'PatrimonyService', iconName: 'more_horiz', route: 'patrimony-service' },
          { displayName: 'PensionRegister', iconName: 'more_horiz', route: 'pension-register' },
          {
            displayName: 'RetrieveTiGroups', iconName: 'more_horiz',
            children: [
              { displayName: 'V1', iconName: 'more_horiz', route: 'retrieve-ti-groups-v1' },
              { displayName: 'V2', iconName: 'more_horiz', route: 'retrieve-ti-groups-v2' }
            ]
          },
          {
            displayName: 'SelfEmployed', iconName: 'more_horiz',
            children: [
              { displayName: 'V1', iconName: 'more_horiz', route: 'self-employed-v1' },
              { displayName: 'V2', iconName: 'more_horiz', route: 'self-employed-v2' }
            ]
          },
          { displayName: 'SocialRateInvestigation', iconName: 'more_horiz', route: 'social-rate-investigation' },
          {
            displayName: 'TaxAssessmentData', iconName: 'more_horiz',
            children: [
              { displayName: 'V1', iconName: 'more_horiz', route: 'tax-assessment-data-v1' },
              { displayName: 'V2', iconName: 'more_horiz', route: 'tax-assessment-data-v2' },
            ]
          },
          {
            displayName: 'UnemploymentData', iconName: 'more_horiz',
            children: [
              { displayName: 'V1', iconName: 'more_horiz', route: 'unemployment-data-v1' },
              { displayName: 'V2', iconName: 'more_horiz', route: 'unemployment-data-v2' },
              { displayName: 'V3', iconName: 'more_horiz', route: 'unemployment-data-v3' }
            ]
          },
        ]
      },
      {
        displayName: 'Help', disabled: true, iconName: 'help_outline',
        children: [
          { displayName: 'Console BCSS', iconName: 'more_horiz', route: 'help' },
        ]
      }
    ];
  }
}


