import { Component, ViewChild, ElementRef, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { VERSION } from '@angular/material';
import { NavigMenu } from './shared/inavigation';
import { NavService } from './shared/nav.service';
import { RouterOutlet } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { slideInAnimation } from './shared/animations';
import { ThemeService } from './shared/theme.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'material-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    slideInAnimation // <-- animation triggers go here
  ],
  providers: [
    { provide: Window, useValue: window }
]
})
export class AppComponent implements AfterViewInit {

  @ViewChild('appDrawer', {static: true}) appDrawer: ElementRef;
  isDarkThemeActive: boolean;
  public version = VERSION;
  public  navList = new NavigMenu().navigList();

  constructor(
    private navService: NavService,
    private cookieService: CookieService,
    public theme: ThemeService) {
      this.theme.OnThemeSwitch.subscribe(value => this.isDarkThemeActive = value);
  }

  public prepareRoute(outlet: RouterOutlet): RouterOutlet {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation; // ['animation'];
  }

  public ngAfterViewInit() {
    this.navService.appDrawer = this.appDrawer;
  }

}
