import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { INavigation,  INavigationParams } from '../shared/inavigation';
import { Router } from '@angular/router';
import { NavService } from '../shared/nav.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-nav-list',
  templateUrl: './nav-list.component.html',
  styleUrls: ['./nav-list.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),
    ])
  ]
})

export class NavListComponent implements OnInit {

  expanded: boolean;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() navig: INavigation;
  @Input() depth: number;
  public cookieValue: any;
  public p: INavigationParams[] = [];

  constructor(public navService: NavService, public router: Router, private cookieService: CookieService) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  ngOnInit() {

    this.navService.currentUrl.subscribe((url: string) => {
      if (this.navig.route && url) {
        console.log(`Checking '/${this.navig.route}' against '${url}'`);
        this.expanded = url.indexOf(`/${this.navig.route}`) === 0;
        this.ariaExpanded = this.expanded;
        console.log('NAV-LIST' + `${this.navig.route} is expanded: ${this.expanded}`);
      }
    });

  }

  public onNavigSelected(navig: INavigation) {
    // const cookieExists: boolean = cookieService.check('test');
    // const allCookies: {} = cookieService.getAll();
    // cookieService.delete('test');
    // cookieService.deleteAll();
    this.cookieService.set( 'Test', JSON.stringify(navig));
    this.cookieValue = this.cookieService.get('Test');

    if ('position' in navig)  {
      console.log('position', navig.position);
      switch (navig.position) {
        case 1:
          this.setParams({context: navig.displayName});
          break;
        case 2:
          this.setParams({env: navig.env});
          break;
        default:
          this.setParams({route: navig.displayName});
      }
    }

    console.log('getParams', this.p);
    if (!navig.children || !navig.children.length) {
      console.log(' SECOND  +++ ');
      if (navig.route === ' ') {

        console.log('navig.route====> ' , navig.displayName );
        this.navService.closeNav();
        return false;
      }
      this.router.navigate([navig.route]);
      this.navService.closeNav();
    }
    if (navig.children && navig.children.length) {
      console.log(' THIRD  +++ ', navig);
      this.expanded = !this.expanded;
    }

  }

  public setParams(params: INavigationParams) {
    this.p.push(params);
  }

}
