import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Cars, Car } from './car.model';
import { Store } from '@ngrx/store';
import { AppState } from './redux/app.state';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public cars: Car[] = [];

  searchInput: FormControl = new FormControl(' ');



  // public carState: Observable<AppState>;

  public carState = new Observable<Cars>();

  constructor ( private store: Store<AppState> ) {

    this.searchInput.
    valueChanges.
    pipe(debounceTime(500)).
    subscribe(
      stock => this.getStock(stock)
    );


   }
   getStock(stock:string) {

    console.log(`${stock} is ${100 * Math.random()}`);
   }

  onKey (event:any) {
    console.log(event.target.value);
  }
  onKeyUp(value:string) {
    console.log( value);
  }

  ngOnInit() {
    // this.store.select('carPage').subscribe( ({cars}) => {
    //   this.cars = cars;
    //   console.log('this is my car !!!', cars);
    // });

    this.carState = this.store.select('carPage'); 
  }

  // onAdd(car: Car): void {

  //   this.cars.push(car);
  // }

  onDelete(car:Car): void {
     
    this.cars = this.cars.filter(c => c.id !==car.id);
  }

}
