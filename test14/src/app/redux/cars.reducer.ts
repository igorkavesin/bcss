import { Action } from '@ngrx/store';
import { Car } from '../car.model';
import { CAR_ACTION, AddCar } from './cars.action';

const initialState = {
    cars: [ 
        new Car('ford', '12.12.12', 'Focus', true, 1),
        new Car('peugeot', '12.12.12', '5008', false, 2)
    ]
}
export function carsReducer( state = initialState, action: AddCar) {

    switch(action.type) {

        case CAR_ACTION.ADD_CAR:

        return {
            ...state, 
            cars: [...state.cars, action.payload]
        };
        default:
            return state;
    }
}