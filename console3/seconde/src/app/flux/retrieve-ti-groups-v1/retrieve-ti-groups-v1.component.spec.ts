import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetrieveTiGroupsV1Component } from './retrieve-ti-groups-v1.component';

describe('RetrieveTiGroupsV1Component', () => {
  let component: RetrieveTiGroupsV1Component;
  let fixture: ComponentFixture<RetrieveTiGroupsV1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetrieveTiGroupsV1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetrieveTiGroupsV1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
