import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange, MatSlideToggle } from '@angular/material';
import { AppSettings, ICON_EDIT } from 'src/app/shared/app-settings';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavService } from 'src/app/shared/nav.service';

export interface INaturePerson {
  value: string;
  viewValue: string;
}
export interface ILanguage {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-cadnet',
  templateUrl: './cadnet.component.html'
})
export class CadnetComponent implements OnInit {

  private data = { };

  public dispalyTitle: string;
  public dispalyDesc = '- D\'envoyer une motivation clignotant à la BCSS. ';
  public hint: boolean;
  public form: FormGroup;
  private pattern: any;
  public iconEdit = ICON_EDIT;

  public navigFull: object;


  natures: INaturePerson[] = [
    {value: '1', viewValue: 'Demandeur'},
    {value: '2', viewValue: 'Cohabitant'},
    {value: '3', viewValue: 'Débiteur alimentaire'}
  ];

  langs: ILanguage[] = [
    {value: 'fr', viewValue: 'Français'},
    {value: 'nl', viewValue: 'Néerlandais'}
  ];


  constructor(public navService: NavService, ) { }

  ngOnInit() {

    this.dispalyTitle = this.navService.getNameFlux();

    setInterval(() => {
      this.navigFull = this.navService.getNav();
    }, 1000);
    const formData = {};
    for (const prop of Object.keys(this.data)) {

      this.pattern = AppSettings.REGEX.NUMBER_SIMPLE;

      formData[prop] = new FormControl(
        this.data[prop],
        [ Validators.required, Validators.pattern(this.pattern) ]
      );

    }
    this.form = new FormGroup(formData);
    
  }

  public doRequest( ): void {}

  private timing(): number | Date {
    return AppSettings.TIMING;
  }

  public btnSend(): void {
      this.doRequest();
  }

  public btnClear(): void {
    this.form.reset();
  }

  public onChange(ob: MatSlideToggleChange): void {
    console.log(ob.checked);
    const matSlideToggle: MatSlideToggle = ob.source;
    console.log(matSlideToggle.color);
    console.log(matSlideToggle.required);
    this.hint = ob.checked;
  }
  
}
