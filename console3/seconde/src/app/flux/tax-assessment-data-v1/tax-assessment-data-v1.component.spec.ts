import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAssessmentDataV1Component } from './tax-assessment-data-v1.component';

describe('TaxAssessmentDataV1Component', () => {
  let component: TaxAssessmentDataV1Component;
  let fixture: ComponentFixture<TaxAssessmentDataV1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxAssessmentDataV1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAssessmentDataV1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
