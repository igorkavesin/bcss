import { Component, OnInit, Input, ɵConsole, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup, AbstractControl, FormArray } from '@angular/forms';
import { MatSlideToggle, MatSlideToggleChange, MatDialog, MAT_CHECKBOX_CLICK_ACTION, NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../../shared/app-settings';
import { INavigation, INavigationParams } from '../../shared/inavigation';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { RequestService } from '../../shared/request.service';

import {AppDateAdapter, APP_DATE_FORMATS } from '../../shared/format-datepicker';

export interface IScope {
  value: string;
  viewValue: string;
}

export interface ICheck {
  value?: string;
  viewValue?: string;
  checked?: boolean;
  check?: string;
}

type btnId = string;

@Component({
  selector: 'app-health-care-insurance',
  templateUrl: './health-care-insurance.component.html',
  styleUrls: ['./health-care-insurance.component.scss'],
  providers: [
    {provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check'},
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR' }
  ]
})

export class HealthCareInsuranceComponent implements OnInit {
  @Input() rn: number;

  public cookieValue: any;
  public chk = false;

  cleanUpField = '';

  public hint = false;
  public loading = false;
  public form: FormGroup;
  private btn: btnId = 'btn_send';

  public desiredData: Array<ICheck> = [
    {value: 'insuringOrganization',  viewValue: 'OA et mutualité',                    checked: false, check: ''},
    {value: 'reimbursementRight',    viewValue: 'Droit au remboursement',             checked: false, check: ''},
    {value: 'ct1ct2',                viewValue: 'Codes CT1/CT2',                      checked: false, check: ''},
    {value: 'payingThirdParty',      viewValue: 'Droit au tiers payant',              checked: false, check: ''},
    {value: 'maximumCharge',         viewValue: 'Année où le plafond a été atteint',  checked: false, check: ''},
    {value: 'medicalHouse',          viewValue: 'Contrats avec maison médicale ',     checked: false, check: ''},
    {value: 'increasedIntervention', viewValue: 'Droit à l\'intervention majorée ',   checked: false, check: ''},
    {value: 'statusComplementaryInsurance', viewValue: 'Assurance complémentaire ',   checked: false, check: ''},
    {value: 'globalMedicalFile',            viewValue: 'Médecin gérant le dossier ',  checked: false, check: ''},
    {value: 'sfdfL891',                     viewValue: 'Données du formulaire L891 ', checked: false, check: ''}
  ];

  public constructor(public req: RequestService, public dialog: MatDialog, private http: HttpClient) {}

    ngOnInit() {

      const formData = {
        rn: new FormControl('', [ Validators.required, Validators.pattern(AppSettings.REGEX.RN) ]),
        datepickerRef: new FormControl(false, [ Validators.required])
      };

      this.form = new FormGroup(formData);
    }

  public actionCheckBox(index: number): Array<ICheck> {
    if ( index !== -1 ) {
      this.desiredData[index].checked = (!this.desiredData[index].checked ? false : true);
    } else {
      this.chk = !this.chk;
      this.desiredData.forEach(( key ) => {
        key.checked = !key.checked;
        key.check = (!key.checked ? '' : 'checked');
      } );
    }
    return this.desiredData;
  }

  public doRequest(): void {
    console.log(this.form);
  }

  public btnSend(): void {
    (document.getElementById(this.btn) as HTMLInputElement).disabled = true;
    this.form.disable();
    this.doRequest();
  }

  public btnClear(): void {
    this.form.reset();
  }

  public onChange(ob: MatSlideToggleChange): void {
    console.log(ob.checked);
    let matSlideToggle: MatSlideToggle = ob.source;
    console.log(matSlideToggle.color);
    console.log(matSlideToggle.required);
    this.hint = ob.checked;

    console.log('====> ');

  }

}





