import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnemploymentDataV3Component } from './unemployment-data-v3.component';

describe('UnemploymentDataV3Component', () => {
  let component: UnemploymentDataV3Component;
  let fixture: ComponentFixture<UnemploymentDataV3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnemploymentDataV3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnemploymentDataV3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
