import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IForm } from '../iform';
import { AppSettings, ICON_BUTTON_TEST, ICON_BUTTON_CLEAR } from '../../shared/app-settings';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { RequestService } from '../request.service';
import { MatDialog } from '@angular/material';

import { NavService } from '../nav.service';

@Component({
  selector: 'app-buttons-test-clear',
  template: `
    <app-progress-bar [loading]="loading"></app-progress-bar>
    <button mat-raised-button color="primary" (click)="btnSend()"
        class="submit-button" id="idsend"
        [disabled]="form.invalid">
      <mat-icon matSuffix>{{iconButtonTest}}</mat-icon> Tester
    </button>

    <button mat-raised-button color="ink-ripple" (click)="btnClear()">
      <mat-icon matSuffix >{{iconButtonClear}}</mat-icon> Effacer
    </button>`,
})
export class ButtonsTestClearComponent implements OnInit{

  @Input() public form: FormGroup;
  @Input() public formApi: IForm;
  private iconButtonTest = ICON_BUTTON_TEST;
  private iconButtonClear = ICON_BUTTON_CLEAR;

  public loading = false;
  private idsend = 'idsend';
  private path: string;

  constructor(public req: RequestService, public dialog: MatDialog, public navService: NavService ) { }

  ngOnInit() {
  }

  private doRequest() {

    this.req.handleTheRequest(AppSettings.URL + this.path).subscribe(
      (response)  => {
        (document.getElementById(this.idsend) as HTMLInputElement).disabled = false;
        this.form.enable();
        this.loading = false;

        const dialogRef = this.dialog.open( ConfirmDialogComponent, {
          data: { bcss: response, full: this.form.get }
        });
        dialogRef.afterClosed();
      },
      (error) => { console.log(' ERROR : ',  error); },
      () => { console.log(' COMPLECT!!! '); }
    );
  }

  public btnSend() {
    this.path = this.formApi.callValueFromInput('');
    if (this.path === 'ERROR') {
      return ;
    }
    (document.getElementById(this.idsend) as HTMLInputElement).disabled = true;
    this.form.disable();
    this.loading = true;
    this.doRequest();
  }

  public btnClear(): void {
    this.form.reset();
  }

}

