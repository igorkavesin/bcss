import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor() { }
  public OnThemeSwitch: Subject<boolean> = new Subject<boolean>();
}
