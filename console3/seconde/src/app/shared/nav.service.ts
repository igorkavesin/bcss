import { Injectable } from '@angular/core';
import { Event, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { NavigMenu, INavigationParams, INavigation } from './inavigation';

@Injectable()
export class NavService {
  public appDrawer: any;
  public currentUrl = new BehaviorSubject<string>(undefined);
  public nameFlux: string;
  public iNavigParams: INavigationParams = {};
  public navigationList = new NavigMenu().navigList();

  constructor(private router: Router) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        const page = event.urlAfterRedirects.substring(1, event.urlAfterRedirects.length);
        this.paramNavigation();

        if (this.iNavigParams.route !== page) {
          this.router.navigate([this.iNavigParams.route]);
        }
        this.setNameFlux(this.iNavigParams.route);
        this.currentUrl.next(event.urlAfterRedirects);
      }
    });
  }

  private paramNavigation(): void {
    this.navigationList.forEach((k, v) => {
      this.navigationList[v].children.filter(x => x.selected === true).map((obj) => {
       if (obj.position === 1 ) {
         this.iNavigParams.context = obj.displayName;
       }
       if (obj.position === 2) {
         this.iNavigParams.env = obj.displayName;
       }
       if (obj.position >= 3 ) {
         this.iNavigParams.description = obj.description;
         this.iNavigParams.route = obj.route;
       }
     }) ;
   });
  }

  public closeNav(): void {
    this.appDrawer.close();
  }

  public openNav(): void {
    this.appDrawer.open();
  }

  public updateNav(navig: INavigation): INavigation {
      const url = this.router.url.substring(1, this.router.url.length);
      if (navig.route === url) {
        return navig;
      }
      this.navigationList[(navig.position - 1)].children.map(
        (obj) => {
          if (obj.displayName === navig.displayName) {
            obj.selected = true;
        } else {
          obj.selected = false;
        } return this.navigationList;
      });
  }

  public getNav(): INavigationParams {
    return this.iNavigParams;
  }

  public setNav(displayName: string, position: number): void {
    if (position === 1) {
      this.iNavigParams.context = displayName;
    }
    if (position === 2) {
      this.iNavigParams.env = displayName;
    }
  }

  public setNameFlux(displayName: string): void {
    this.nameFlux = this.titleCase(displayName);
  }

  public getNameFlux(): string {
     return this.nameFlux;
  }

  public titleCase(displayName: string): string {
    if (displayName === undefined) {
      return '';
    }
    const toTitleCase = (name) => {
      return name
        .toLowerCase()
        .split('-')
        .map(flux => flux.charAt(0).toUpperCase() + flux.slice(1))
        .join('');
    };
    return toTitleCase(displayName);
  }

}
