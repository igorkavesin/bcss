import { ICON_LEGAL_CONTEXT, ICON_FLUX, ICON_MORE, ICON_ENV, ICON_HELP } from '../shared/app-settings';

export interface INavigationParams {
  env?: string;
  context?: string;
  route?: string;
  position?: string;
  description?: string;
}
export interface INavigation {
    displayName: string;
    disabled?: boolean;
    selected?: boolean;
    iconName: string;
    route?: string;
    position?: number;
    description?: string;
    children?: INavigation[];
}

export class NavigMenu {

  navigList(): INavigation[] {
    return [
      {
        displayName: 'Legal Context', iconName: ICON_LEGAL_CONTEXT, children: [
          { displayName: 'PCSA:SOCIAL_INQUIRY', iconName: ICON_MORE, route: ' ', position: 1},
          { displayName: 'PCSA:MANAGE_ECARMED', selected: true, iconName: ICON_MORE, route: ' ', position: 1},
          { displayName: 'SOCIAL_INVESTIGATION_OF_PCSA', iconName: ICON_MORE, route: ' ', position: 1},
        ]
      },
      {
        displayName: 'Environements', iconName: ICON_ENV, children: [
          { displayName: 'Production', selected: true, iconName: ICON_MORE,  route: ' ', position: 2 },
          { displayName: 'Acceptation', iconName: ICON_MORE, route: ' ', position: 2 },
          { displayName: 'Test',  iconName: ICON_MORE, route: ' ', position: 2 }
        ]
      },
      {
        displayName: 'Flux', iconName: ICON_FLUX,
        children: [
          {
            displayName: 'AlertReaction', iconName: ICON_MORE, route: 'alert-reaction', position: 3,
            description: `- D\'envoyer une motivation clignotant à la BCSS. `,
            selected: true
          },
          {
            displayName: 'Cadnet', iconName: ICON_MORE, route: 'cadnet', position: 3,
            description: `- Données cadastrales de la personne.`
          },
          { displayName: 'ChildBenefits', iconName: ICON_MORE, route: 'child-benefits', position: 3},
          { displayName: 'Dimona', iconName: ICON_MORE, route: 'dimona', position: 3},
          { displayName: 'Dolsis', iconName: ICON_MORE, route: 'dolsis', position: 3},
          { displayName: 'FamilyAllowancesService', iconName: ICON_MORE, route: 'family-allowances-service', position: 3},
          { displayName: 'HandiFlux', iconName: ICON_MORE, route: 'handi-flux', position: 3},
          { displayName: 'HealthCareInsurance', iconName: ICON_MORE, route: 'health-care-insurance', position: 3},
          { displayName: 'IdentifyPerson', iconName: ICON_MORE, route: 'identify-person', position: 3},
          { displayName: 'ListOfAttestation', iconName: ICON_MORE, route: 'list-of-attestation', position: 3},
          { displayName: 'LivingWages', iconName: ICON_MORE, route: 'living-wages', position: 3},
          { displayName: 'ManageAccess', iconName: ICON_MORE, route: 'manage-access', position: 3},
          { displayName: 'PatrimonyService', iconName: ICON_MORE, route: 'patrimony-service', position: 3},
          { displayName: 'PensionRegister', iconName: ICON_MORE, route: 'pension-register', position: 3},
          {
            displayName: 'RetrieveTiGroups', iconName: ICON_MORE,
            children: [
              { displayName: 'V1', iconName: ICON_MORE, route: 'retrieve-ti-groups-v1', position: 3},
              { displayName: 'V2', iconName: ICON_MORE, route: 'retrieve-ti-groups-v2', position: 3}
            ]
          },
          {
            displayName: 'SelfEmployed', iconName: ICON_MORE,
            children: [
              { displayName: 'V1', iconName: ICON_MORE, route: 'self-employed-v1', position: 3},
              { displayName: 'V2', iconName: ICON_MORE, route: 'self-employed-v2', position: 3}
            ]
          },
          { displayName: 'SocialRateInvestigation', iconName: ICON_MORE, route: 'social-rate-investigation', position: 3},
          {
            displayName: 'TaxAssessmentData', iconName: ICON_MORE,
            children: [
              { displayName: 'V1', iconName: ICON_MORE, route: 'tax-assessment-data-v1', position: 3},
              { displayName: 'V2', iconName: ICON_MORE, route: 'tax-assessment-data-v2', position: 3},
            ]
          },
          {
            displayName: 'UnemploymentData', iconName: ICON_MORE,
            children: [
              { displayName: 'V1', iconName: ICON_MORE, route: 'unemployment-data-v1', position: 3},
              { displayName: 'V2', iconName: ICON_MORE, route: 'unemployment-data-v2', position: 3},
              { displayName: 'V3', iconName: ICON_MORE, route: 'unemployment-data-v3', position: 3}
            ]
          },
        ]
      },
      {
        displayName: 'Help', disabled: true, iconName: ICON_HELP,
        children: [
          { displayName: 'Console BCSS', iconName: ICON_MORE, route: 'help',  position: 4 },
        ]
      }
    ];
  }
   
}


