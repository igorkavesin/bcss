import { Component, HostListener, ViewChild, ElementRef, ViewEncapsulation, AfterViewInit, Inject} from '@angular/core';
import { VERSION } from '@angular/material';
import { INavigation, INavigationParams } from './shared/inavigation';
import { NavService } from './shared/nav.service';
import { RouterOutlet } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { slideInAnimation } from './shared/animations';
import { ThemeService } from './shared/theme.service';


@Component({
  // tslint:disable-next-line: component-selector
  selector: 'material-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    slideInAnimation // <-- animation triggers go here
  ],
  providers: [
    { provide: Window, useValue: window }
]
})

export class AppComponent implements AfterViewInit {
   
  @ViewChild('appDrawer', {static: true}) appDrawer: ElementRef;
  isDarkThemeActive: boolean;
  public version = VERSION;

  public navList: INavigation[] = [
    {
      displayName: 'Legal Context', iconName: 'shuffle', children: [
        {
          displayName: 'PCSA:SOCIAL_INQUIRY', iconName: 'more_horiz', route: ' ', position: 1
        },
        {
          displayName: 'PCSA:MANAGE_ECARMED', iconName: 'more_horiz', route: ' ', position: 1
        },
        {
          displayName: 'SOCIAL_INVESTIGATION_OF_PCSA', iconName: 'more_horiz', route: ' ', position: 1
        },
      ]
    },
    {
      displayName: 'Environements', iconName: 'share', children: [
        {
          displayName: 'Production', iconName: 'more_horiz',  route: ' ', position: 2, env: 'prod',
        },
        {
          displayName: 'Acceptation', iconName: 'more_horiz', route: ' ', position: 2, env: 'acpt',
        },
        {
          displayName: 'Test',  iconName: 'more_horiz', route: ' ', position: 2, env: 'test',
        }
      ]
    },
    {
      displayName: 'Flux', iconName: 'compare_arrows',
      children: [
        {
          displayName: 'AlertReaction', iconName: 'more_horiz', route: 'alert-reaction'
        },
        {
          displayName: 'Cadnet', iconName: 'more_horiz', route: 'cadnet'
        },
        {
          displayName: 'ChildBenefits', iconName: 'more_horiz', route: 'child-benefits'
        },
        {
          displayName: 'Dimona', iconName: 'more_horiz', route: 'dimona'
        },
        {
          displayName: 'Dolsis', iconName: 'more_horiz', route: 'dolsis'
        },
        {
          displayName: 'FamilyAllowancesService', iconName: 'more_horiz', route: 'family-allowances-service'
        },
        {
          displayName: 'HandiFlux', iconName: 'more_horiz', route: 'handi-flux'
        },
        {
          displayName: 'HealthCareInsurance', iconName: 'more_horiz', route: 'health-care-insurance'
        },
        {
          displayName: 'IdentifyPerson', iconName: 'more_horiz', route: 'identify-person'
        },
        {
          displayName: 'ListOfAttestation', iconName: 'more_horiz', route: 'list-of-attestation'
        },
        {
          displayName: ' LivingWages', iconName: 'more_horiz', route: 'living-wages'
        },
        {
          displayName: 'ManageAccess', iconName: 'more_horiz', route: 'manage-access'
        },
        {
          displayName: 'PatrimonyService', iconName: 'more_horiz', route: 'patrimony-service'
        },
        {
          displayName: 'PensionRegister', iconName: 'more_horiz', route: 'pension-register'
        },
        {
          displayName: 'RetrieveTiGroups', iconName: 'more_horiz',
          children: [
            {
              displayName: 'V1', iconName: 'more_horiz', route: 'retrieve-ti-groups-v1'
            },
            {
              displayName: 'V2', iconName: 'more_horiz', route: 'retrieve-ti-groups-v2'
            }
          ]
        },
        {
          displayName: 'SelfEmployed', iconName: 'more_horiz',
          children: [
            {
              displayName: 'V1', iconName: 'more_horiz', route: 'self-employed-v1'
            },
            {
              displayName: 'V2', iconName: 'more_horiz', route: 'self-employed-v2'
            }
          ]
        },
        {
          displayName: 'SocialRateInvestigation', iconName: 'more_horiz', route: 'social-rate-investigation'
        },
        {
          displayName: 'TaxAssessmentData', iconName: 'more_horiz',
          children: [
            {
              displayName: 'V1', iconName: 'more_horiz', route: 'tax-assessment-data-v1'
            },
            {
              displayName: 'V2', iconName: 'more_horiz', route: 'tax-assessment-data-v2'
            },
          ]
        },
        {
          displayName: 'UnemploymentData', iconName: 'more_horiz',
          children: [
            {
              displayName: 'V1', iconName: 'more_horiz', route: 'unemployment-data-v1'
            },
            {
              displayName: 'V2', iconName: 'more_horiz', route: 'unemployment-data-v2'
            },
            {
              displayName: 'V3', iconName: 'more_horiz', route: 'unemployment-data-v3'
            }
          ]
        },
      ]
    },
    {
      displayName: 'Help', disabled: true, iconName: 'help_outline',
      children: [
        {
          displayName: 'Console BCSS', iconName: 'more_horiz', route: 'help/'
        },
        {
          displayName: 'Information', iconName: 'more_horiz', route: 'help/'
        }
      ]
    }
  ];
  constructor(private navService: NavService, private cookieService: CookieService , public theme: ThemeService) { 

    this.theme.OnThemeSwitch.subscribe(value => this.isDarkThemeActive = value);

  }

  public prepareRoute(outlet: RouterOutlet): RouterOutlet {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation; // ['animation'];
  }

  public ngAfterViewInit() {
    this.navService.appDrawer = this.appDrawer;
  }

}
