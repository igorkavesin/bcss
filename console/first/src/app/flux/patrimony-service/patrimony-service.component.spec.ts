import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatrimonyServiceComponent } from './patrimony-service.component';

describe('PatrimonyServiceComponent', () => {
  let component: PatrimonyServiceComponent;
  let fixture: ComponentFixture<PatrimonyServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatrimonyServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatrimonyServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
