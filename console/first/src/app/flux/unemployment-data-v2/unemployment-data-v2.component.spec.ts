import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnemploymentDataV2Component } from './unemployment-data-v2.component';

describe('UnemploymentDataV2Component', () => {
  let component: UnemploymentDataV2Component;
  let fixture: ComponentFixture<UnemploymentDataV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnemploymentDataV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnemploymentDataV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
