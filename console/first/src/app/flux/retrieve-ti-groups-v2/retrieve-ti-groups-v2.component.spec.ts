import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetrieveTiGroupsV2Component } from './retrieve-ti-groups-v2.component';

describe('RetrieveTiGroupsV2Component', () => {
  let component: RetrieveTiGroupsV2Component;
  let fixture: ComponentFixture<RetrieveTiGroupsV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetrieveTiGroupsV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetrieveTiGroupsV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
