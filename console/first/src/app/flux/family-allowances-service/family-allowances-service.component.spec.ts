import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyAllowancesServiceComponent } from './family-allowances-service.component';

describe('FamilyAllowancesServiceComponent', () => {
  let component: FamilyAllowancesServiceComponent;
  let fixture: ComponentFixture<FamilyAllowancesServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyAllowancesServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyAllowancesServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
