import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DolsisComponent } from './dolsis.component';

describe('DolsisComponent', () => {
  let component: DolsisComponent;
  let fixture: ComponentFixture<DolsisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DolsisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DolsisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
