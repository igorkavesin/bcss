import { Component, OnInit, Input, ɵConsole, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { MatSlideToggle, MatSlideToggleChange, MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../../shared/app-settings';
import { INavigation, INavigationParams } from '../../shared/inavigation';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { RequestService } from '../../shared/request.service';

@Component({
  selector: 'app-alert-reaction',
  templateUrl: './alert-reaction.component.html',
  styleUrls: ['./alert-reaction.component.scss']
})

export class AlertReactionComponent implements OnInit {
    @Input() alertid: number;
    @Input() reference: number;
    @Input() motivation: number;
    @Input() comment: number;

    public cookieValue: any;

    private data = {
      alertid:    this.alertid,
      reference:  this.reference,
      motivation: this.motivation,
      comment:    this.comment
    };

    public hint = false;
    public loading = false;
    private form: FormGroup;
    private pattern: any;
    private btn: string = 'btn_send';

  public constructor(public req: RequestService, public dialog: MatDialog, private http: HttpClient, private router: Router) { }

  public ngOnInit(): void {
    const formData = {};
    for (const prop of Object.keys(this.data)) {
      this.pattern = (prop === 'comment' ? AppSettings.REGEX.STRING_SIMPLE : AppSettings.REGEX.NUMBER_SIMPLE);
      formData[prop] = new FormControl(
        this.data[prop],
        [ Validators.required, Validators.pattern(this.pattern) ]
      );
    }
    this.form = new FormGroup(formData);
  }

  public doRequest(): void {
    const path = 'alert_reaction/consulter/?env=prod' +
                  '&alertId=' + this.form.get('alertid').value +
                  '&formNumber=' + this.form.get('reference').value +
                  '&codeM=' + this.form.get('motivation').value +
                  '&comment=' + this.form.get('comment').value ;

    this.loading = true;

    this.req.handleTheRequest(AppSettings.URL + path).subscribe(
      (response)  => {
        (document.getElementById(this.btn) as HTMLInputElement).disabled = false;
        this.form.enable();
        this.loading = false;
        const dialogRef = this.dialog.open( ConfirmDialogComponent, {
          data: { bcss: response, full: this.form.get }
        });
        dialogRef.afterClosed();
      },
      (error) => { console.log(' ERROR : ',  error); },
      () => { console.log(' COMPLECT!!! '); }
    );
  }

  public btnSend(): void {
    (document.getElementById(this.btn) as HTMLInputElement).disabled = true;
    this.form.disable();
    this.doRequest();
  }

  public btnClear(): void {
    this.form.reset();
  }

  public onChange(ob: MatSlideToggleChange): void {
    console.log(ob.checked);
    const matSlideToggle: MatSlideToggle = ob.source;
    console.log(matSlideToggle.color);
    console.log(matSlideToggle.required);
    this.hint = ob.checked;
  }

  public getParamsy(): INavigationParams[] {
    const params: INavigationParams[] = [];
    const add: INavigationParams = {context: 'test', env: 'asdasd', route: ''};
    params.push(add);
    return params;
  }

}
