import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange, MatSlideToggle } from '@angular/material';
import { AppSettings } from 'src/app/shared/app-settings';
import { FormGroup, FormControl, Validators } from '@angular/forms';

export interface INaturePerson {
  value: string;
  viewValue: string;
}
export interface ILanguage {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-cadnet',
  templateUrl: './cadnet.component.html',
  styleUrls: ['./cadnet.component.scss']
})
export class CadnetComponent implements OnInit {

  private data = { };

  public hint = false;
  public loading = false;
  private form: FormGroup;
  private pattern: any;

  natures: INaturePerson[] = [
    {value: '1', viewValue: 'Demandeur'},
    {value: '2', viewValue: 'Cohabitant'},
    {value: '3', viewValue: 'Débiteur alimentaire'}
  ];

  langs: ILanguage[] = [
    {value: 'fr', viewValue: 'Français'},
    {value: 'nl', viewValue: 'Néerlandais'}
  ];


  constructor() { }

  ngOnInit() {

    const formData = {};
    for (const prop of Object.keys(this.data)) {

      this.pattern = AppSettings.REGEX.NUMBER_SIMPLE;

      formData[prop] = new FormControl(
        this.data[prop],
        [ Validators.required, Validators.pattern(this.pattern) ]
      );

    }
    this.form = new FormGroup(formData);
    
  }

  public doRequest( ): void {}

  private timing(): number | Date {
    return AppSettings.TIMING;
  }

  public btnSend(): void {
      this.doRequest();
  }

  public btnClear(): void {
    this.form.reset();
  }

  public onChange(ob: MatSlideToggleChange): void {
    console.log(ob.checked);
    const matSlideToggle: MatSlideToggle = ob.source;
    console.log(matSlideToggle.color);
    console.log(matSlideToggle.required);
    this.hint = ob.checked;
  }
  
}
