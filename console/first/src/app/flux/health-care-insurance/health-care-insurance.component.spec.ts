import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthCareInsuranceComponent } from './health-care-insurance.component';

describe('HealthCareInsuranceComponent', () => {
  let component: HealthCareInsuranceComponent;
  let fixture: ComponentFixture<HealthCareInsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthCareInsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthCareInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
