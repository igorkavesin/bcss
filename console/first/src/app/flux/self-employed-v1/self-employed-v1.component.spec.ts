import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfEmployedV1Component } from './self-employed-v1.component';

describe('SelfEmployedV1Component', () => {
  let component: SelfEmployedV1Component;
  let fixture: ComponentFixture<SelfEmployedV1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfEmployedV1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfEmployedV1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
