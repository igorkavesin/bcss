import {Injectable} from '@angular/core';
import {Event, NavigationEnd, Router} from '@angular/router';
import {BehaviorSubject, Observable, throwError, of} from 'rxjs';
import { HttpClient, HttpHeaders, HttpClientModule, HttpParams } from '@angular/common/http';
import { map, mergeMap, delay, catchError } from 'rxjs/operators';
import { AppSettings } from './app-settings';

@Injectable()
export class RequestService {
  
    public appDrawer: any;
    public currentUrl = new BehaviorSubject<string>(undefined);
    private data: Object = {};

  constructor(private router: Router, private http: HttpClient) {

  }

  public handleTheRequest(url: string): Observable<any> {

    return this.http.get<any>(url).pipe(
      delay(this.timing()),
      catchError(err => throwError(err)),
      catchError(err => of(err)),
      map( (x: Object[] ) => {
        return x;
      } )
    );

  }

  private timing(): number | Date {
    return AppSettings.TIMING;
  }

}
