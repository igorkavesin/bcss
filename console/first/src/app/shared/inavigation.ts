export interface INavigationParams {
  env?: string;
  context?: string;
  route?: string;
}
export interface INavigation {
    displayName: string;
    disabled?: boolean;
    iconName: string;
    route?: string;
    position?: number;
    env?: string;
    children?: INavigation[];
  }


export class MenuNavig implements INavigationParams {}



