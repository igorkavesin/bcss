import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {CommonModule, JsonPipe, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr-FR');
import {NgxJsonViewerModule} from 'ngx-json-viewer';

import {ConfirmDialogComponent} from './shared/confirm-dialog/confirm-dialog.component';
import {PrettyJsonModule} from 'angular2-prettyjson';

import {A11yModule} from '@angular/cdk/a11y';
import {BidiModule} from '@angular/cdk/bidi';
import {ObserversModule} from '@angular/cdk/observers';
import {OverlayModule} from '@angular/cdk/overlay';
import {PlatformModule} from '@angular/cdk/platform';
import {PortalModule} from '@angular/cdk/portal';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import { OverlayContainer } from '@angular/cdk/overlay';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CookieService } from 'ngx-cookie-service';
import { AppRoutingModule } from './app-routing.module';
import { NavService } from './shared/nav.service';
import { RequestService } from './shared/request.service';
import { NavTopComponent } from './nav-top/nav-top.component';
import { NavListComponent } from './nav-list/nav-list.component';

import { AlertReactionComponent } from './flux/alert-reaction/alert-reaction.component';
import { CadnetComponent } from './flux/cadnet/cadnet.component';
import { ChildBenefitsComponent } from './flux/child-benefits/child-benefits.component';
import { DimonaComponent } from './flux/dimona/dimona.component';
import { DolsisComponent } from './flux/dolsis/dolsis.component';
import { FamilyAllowancesServiceComponent } from './flux/family-allowances-service/family-allowances-service.component';
import { HandiFluxComponent } from './flux/handi-flux/handi-flux.component';
import { HealthCareInsuranceComponent } from './flux/health-care-insurance/health-care-insurance.component';
import { IdentifyPersonComponent } from './flux/identify-person/identify-person.component';
import { ListOfAttestationComponent } from './flux/list-of-attestation/list-of-attestation.component';
import { LivingWagesComponent } from './flux/living-wages/living-wages.component';
import { ManageAccessComponent } from './flux/manage-access/manage-access.component';
import { PatrimonyServiceComponent } from './flux/patrimony-service/patrimony-service.component';
import { PensionRegisterComponent } from './flux/pension-register/pension-register.component';
import { RetrieveTiGroupsV1Component } from './flux/retrieve-ti-groups-v1/retrieve-ti-groups-v1.component';
import { RetrieveTiGroupsV2Component } from './flux/retrieve-ti-groups-v2/retrieve-ti-groups-v2.component';
import { SelfEmployedV1Component } from './flux/self-employed-v1/self-employed-v1.component';
import { SelfEmployedV2Component } from './flux/self-employed-v2/self-employed-v2.component';
import { SocialRateInvestigationComponent } from './flux/social-rate-investigation/social-rate-investigation.component';
import { TaxAssessmentDataV1Component } from './flux/tax-assessment-data-v1/tax-assessment-data-v1.component';
import { TaxAssessmentDataV2Component } from './flux/tax-assessment-data-v2/tax-assessment-data-v2.component';
import { UnemploymentDataV1Component } from './flux/unemployment-data-v1/unemployment-data-v1.component';
import { UnemploymentDataV2Component } from './flux/unemployment-data-v2/unemployment-data-v2.component';
import { UnemploymentDataV3Component } from './flux/unemployment-data-v3/unemployment-data-v3.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
/**
 * NgModule that includes all Material modules that are required.
 */
@NgModule({
  exports: [
    RouterModule,
    // CDK
    A11yModule,
    BidiModule,
    ObserversModule,
    OverlayModule,
    PlatformModule,
    PortalModule,
    CdkStepperModule,
    CdkTableModule,
    // Material
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatSnackBarModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatNativeDateModule,
    DragDropModule
  ],
  declarations: [ ]
})
export default class MaterialModule {}

@NgModule({
  imports: [
    PrettyJsonModule,
    NgxJsonViewerModule,
    BrowserModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AppRoutingModule,
    HttpClientModule,
    PrettyJsonModule,
    DragDropModule
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  declarations: [
    AppComponent,
    ConfirmDialogComponent,
    NavListComponent,
    NavTopComponent,
    AlertReactionComponent,
    CadnetComponent,
    ChildBenefitsComponent,
    DimonaComponent,
    UnemploymentDataV1Component,
    UnemploymentDataV2Component,
    UnemploymentDataV3Component,
    DolsisComponent,
    FamilyAllowancesServiceComponent,
    HandiFluxComponent,
    HealthCareInsuranceComponent,
    IdentifyPersonComponent,
    ListOfAttestationComponent,
    LivingWagesComponent,
    ManageAccessComponent,
    PatrimonyServiceComponent,
    PensionRegisterComponent,
    RetrieveTiGroupsV1Component,
    RetrieveTiGroupsV2Component,
    SelfEmployedV1Component,
    SelfEmployedV2Component,
    SocialRateInvestigationComponent,
    TaxAssessmentDataV1Component,
    TaxAssessmentDataV2Component
  ],
  bootstrap: [AppComponent],
  providers:  [OverlayContainer, NavService, RequestService, CookieService, [{provide: JsonPipe, useClass: HashLocationStrategy }]]
})
export class AppModule {
  constructor(overlayContainer: OverlayContainer) {
    overlayContainer.getContainerElement().classList.add('bcss-dark-theme');
  }
}
