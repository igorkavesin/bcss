import { Component } from '@angular/core';
import { helloState } from '../app/redux/reducers/helloReducer';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'test11';

  constructor(public store: Store<helloState>) { 

    store.subscribe(result => this.title = result.msg)
    console.log(this.title);
  }

  onAddHello()
  {
      this.store.dispatch({ type: "ADD_HELLO" })
  }
  onAddWorld()
  {
      this.store.dispatch({ type: "ADD_WORLD" })
  }
  onRemoveHelloWorld()
  {
      this.store.dispatch({ type: "REMOVE_HELLO_WORLD" })
  }

}
