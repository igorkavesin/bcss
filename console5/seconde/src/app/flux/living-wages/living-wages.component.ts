import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-living-wages',
  templateUrl: './living-wages.component.html'
})
export class LivingWagesComponent implements OnInit {

  public tab: any;
  public navLinks: any[];
  public _activeLinkIndex  = -1;
  public messageContent: string;

  public get activeLinkIndex() {
    return this._activeLinkIndex;
  }

  public set activeLinkIndex(value) {
    this._activeLinkIndex = value;
  }

  constructor() {
    this.messageContent = 'Aucun contenu. la page est en construction!';
  }

  ngOnInit() {
  }

  tabChanged(event: any) {
    console.log(event);
  }


}
