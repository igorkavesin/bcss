import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule, JsonPipe, HashLocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr-FR');

import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { OverlayContainer } from '@angular/cdk/overlay';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';

import { NavService } from './shared/nav.service';
import { RequestService } from './shared/request.service';
import { ConfirmDialogComponent } from './shared/confirm-dialog/confirm-dialog.component';
import { NavTopComponent } from './nav-top/nav-top.component';
import { NavListComponent } from './nav-list/nav-list.component';
import { AlertReactionComponent } from './flux/alert-reaction/alert-reaction.component';
import { CadnetComponent } from './flux/cadnet/cadnet.component';
import { ChildBenefitsComponent } from './flux/child-benefits/child-benefits.component';
import { DimonaComponent } from './flux/dimona/dimona.component';
import { DolsisComponent } from './flux/dolsis/dolsis.component';
import { FamilyAllowancesServiceComponent } from './flux/family-allowances-service/family-allowances-service.component';
import { HandiFluxComponent } from './flux/handi-flux/handi-flux.component';
import { HealthCareInsuranceComponent } from './flux/health-care-insurance/health-care-insurance.component';
import { IdentifyPersonComponent } from './flux/identify-person/identify-person.component';
import { ListOfAttestationComponent } from './flux/list-of-attestation/list-of-attestation.component';
import { LivingWagesComponent } from './flux/living-wages/living-wages.component';
import { ManageAccessComponent } from './flux/manage-access/manage-access.component';
import { PatrimonyServiceComponent } from './flux/patrimony-service/patrimony-service.component';
import { PensionRegisterComponent } from './flux/pension-register/pension-register.component';
import { RetrieveTiGroupsV1Component } from './flux/retrieve-ti-groups-v1/retrieve-ti-groups-v1.component';
import { RetrieveTiGroupsV2Component } from './flux/retrieve-ti-groups-v2/retrieve-ti-groups-v2.component';
import { SelfEmployedV1Component } from './flux/self-employed-v1/self-employed-v1.component';
import { SelfEmployedV2Component } from './flux/self-employed-v2/self-employed-v2.component';
import { SocialRateInvestigationComponent } from './flux/social-rate-investigation/social-rate-investigation.component';
import { TaxAssessmentDataV1Component } from './flux/tax-assessment-data-v1/tax-assessment-data-v1.component';
import { TaxAssessmentDataV2Component } from './flux/tax-assessment-data-v2/tax-assessment-data-v2.component';
import { UnemploymentDataV1Component } from './flux/unemployment-data-v1/unemployment-data-v1.component';
import { UnemploymentDataV2Component } from './flux/unemployment-data-v2/unemployment-data-v2.component';
import { UnemploymentDataV3Component } from './flux/unemployment-data-v3/unemployment-data-v3.component';
import { SlideToggleErrorComponent } from './shared/components/slide-toggle-error.component';
import { TitleAndDescriptionComponent } from './shared/components/title-and-description.component';
import { ProgressBarComponent } from './shared/components/progress-bar.component';
import { ButtonsTestClearComponent } from './shared/components/buttons-test-clear.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HelpComponent } from './flux/help/help.component';
import { InputRnComponent } from './shared/components/input-rn';
import { MaterialModule } from './modules/material/material.module';
import { ActivationPaidSumsComponent } from './flux/unemployment-data-v3/activation-paid-sums/activation-paid-sums.component';
import { ExemptionHistoryComponent } from './flux/unemployment-data-v3/exemption-history/exemption-history.component';
import { NoEarningCapacityComponent } from './flux/unemployment-data-v3/no-earning-capacity/no-earning-capacity.component';
import { PaidSumsComponent } from './flux/unemployment-data-v3/paid-sums/paid-sums.component';
import { PaymentsComponent } from './flux/unemployment-data-v3/payments/payments.component';
import { RightsComponent } from './flux/unemployment-data-v3/rights/rights.component';
import { SanctionHistoryComponent } from './flux/unemployment-data-v3/sanction-history/sanction-history.component';
import { ScaleCodeHistoryComponent } from './flux/unemployment-data-v3/scale-code-history/scale-code-history.component';
import { ScheduledPaymentComponent } from './flux/unemployment-data-v3/scheduled-payment/scheduled-payment.component';
import { WorkDisabilityComponent } from './flux/unemployment-data-v3/work-disability/work-disability.component';
import { YoungAvailabilityComponent } from './flux/unemployment-data-v3/young-availability/young-availability.component';
import { CarriereComponent } from './flux/self-employed-v2/carriere/carriere.component';
import { ContributionsComponent } from './flux/self-employed-v2/contributions/contributions.component';
import { MultiDatepickerModule } from './shared/multidatepicker/multidatepicker.module';
import { MediprimaV1Component } from './flux/mediprima-v1/mediprima-v1.component';
import { MediprimaV2Component } from './flux/mediprima-v2/mediprima-v2.component';
import { PrimeInstallationComponent } from './flux/list-of-attestation/prime-installation/prime-installation.component';
import { A036Component } from './flux/list-of-attestation/a036/a036.component';
import { ExonerationsComponent } from './flux/list-of-attestation/exonerations/exonerations.component';
import { ExonerationsArt35Component } from './flux/list-of-attestation/exonerations-art35/exonerations-art35.component';
import { FormulairesComponent } from './flux/list-of-attestation/formulaires/formulaires.component';
import { PiisComponent } from './flux/list-of-attestation/piis/piis.component';
import { Statut65Component } from './flux/list-of-attestation/statut65/statut65.component';
import { LWComponent } from './flux/living-wages/lw/lw.component';
import { LWPeriodsComponent } from './flux/living-wages/lw-periods/lw-periods.component';
import { LWPeriodsFlandersComponent } from './flux/living-wages/lw-periods-flanders/lw-periods-flanders.component';
import { LWPeriodsPerPCSAComponent } from './flux/living-wages/lw-periods-per-pcsa/lw-periods-per-pcsa.component';
import { LWPeriodsPerPCSAFlandersComponent } from './flux/living-wages/lw-periods-per-pcsa-flanders/lw-periods-per-pcsa-flanders.component';
import { FindDivisionByEntNumberComponent } from './flux/patrimony-service/division-by-ent-number/division-by-ent-number.component';
import { EntImmovablePropertiesComponent } from './flux/patrimony-service/ent-immovable-properties/ent-immovable-properties.component';
import { ImmovablePropertiesComponent } from './flux/patrimony-service/immovable-properties/immovable-properties.component';

@NgModule({
  imports: [
    RouterModule,
    PrettyJsonModule,
    NgxJsonViewerModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    MultiDatepickerModule
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  declarations: [
    AppComponent,
    ConfirmDialogComponent,
    NavListComponent,
    NavTopComponent,
    AlertReactionComponent,
    CadnetComponent,
    ChildBenefitsComponent,
    DimonaComponent,
    UnemploymentDataV1Component,
    UnemploymentDataV2Component,
    UnemploymentDataV3Component,
    DolsisComponent,
    FamilyAllowancesServiceComponent,
    HandiFluxComponent,
    HealthCareInsuranceComponent,
    IdentifyPersonComponent,
    ListOfAttestationComponent,
    LivingWagesComponent,
    ManageAccessComponent,
    PatrimonyServiceComponent,
    PensionRegisterComponent,
    RetrieveTiGroupsV1Component,
    RetrieveTiGroupsV2Component,
    SelfEmployedV1Component,
    SelfEmployedV2Component,
    SocialRateInvestigationComponent,
    TaxAssessmentDataV1Component,
    TaxAssessmentDataV2Component,
    SlideToggleErrorComponent,
    TitleAndDescriptionComponent,
    InputRnComponent,
    ProgressBarComponent,
    ButtonsTestClearComponent,
    NotFoundComponent,
    HelpComponent,
    ActivationPaidSumsComponent,
    ExemptionHistoryComponent,
    NoEarningCapacityComponent,
    PaidSumsComponent,
    PaymentsComponent,
    RightsComponent,
    SanctionHistoryComponent,
    ScaleCodeHistoryComponent,
    ScheduledPaymentComponent,
    WorkDisabilityComponent,
    YoungAvailabilityComponent,

    CarriereComponent,
    ContributionsComponent,

    MediprimaV1Component,
    MediprimaV2Component,

    A036Component,
    ExonerationsComponent,
    ExonerationsArt35Component,
    FormulairesComponent,
    PiisComponent,
    PrimeInstallationComponent,
    Statut65Component,

    LWComponent,
    LWPeriodsComponent,
    LWPeriodsFlandersComponent,
    LWPeriodsPerPCSAComponent,
    LWPeriodsPerPCSAFlandersComponent,

    FindDivisionByEntNumberComponent,
    EntImmovablePropertiesComponent,
    ImmovablePropertiesComponent

  ],
  bootstrap: [AppComponent],
  providers:  [
    OverlayContainer,
    NavService,
    RequestService,
    [
      { provide: JsonPipe, useClass: HashLocationStrategy }
    ]
  ]
})
export class AppModule {
  constructor(overlayContainer: OverlayContainer) {
    overlayContainer.getContainerElement().classList.add('bcss-dark-theme');
  }
}
