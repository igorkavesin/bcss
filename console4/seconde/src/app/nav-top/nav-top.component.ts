import { Component, OnInit } from '@angular/core';
import { NavService } from '../shared/nav.service';
import { ThemeService } from '../shared/theme.service';

@Component({
  selector: 'app-nav-top',
  templateUrl: './nav-top.component.html'
})
export class NavTopComponent implements OnInit {
  constructor(public navService: NavService, public theme: ThemeService) { }

  ngOnInit() {
  }

  onThemeChange(event) {
    this.theme.OnThemeSwitch.next(event.checked);
  }

}
